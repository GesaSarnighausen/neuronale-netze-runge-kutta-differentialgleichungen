import matplotlib.pyplot as plt
import numpy as np
import ODEsolveClass as ODE
from scipy.integrate import solve_ivp



# fun1 und fun2 und switch sind für RK mit Event
# u[0] > x_stern
def fun1(t, u, x_stern, d, k, m):
    return np.array([u[1], -(k + 10000)/m * u[0] - (d + 2000)/m * u[1] + 10000 * x_stern/m + ((
                    1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t))/m])


# u[0] <= x_stern
def fun2(t, u, x_stern, d, k, m):
    return np.array([u[1], -k / m * u[0] - d / m * u[1] + (1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t)])

def switch(t, u, x_stern):
    return u[0] - x_stern


# für RK45 ohne Event
def fun(t, u, x_stern, d=2, k=10, m=1):
    if u[0] > x_stern:
        return np.array([u[1], -(k + 10000)/m * u[0] - (d + 2000)/m * u[1] + 10000 * x_stern/m + ((
                    1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t))/m])
    # u[0] <= x_stern
    return np.array([u[1], -k / m * u[0] - d / m * u[1] + (1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t)])

x_stern = 0.033
d = 2
k = 10
m = 1
t0 = 0
u0 = [0, 0]
t_end = 1
tol_array = [1e-5, 7.5e-6, 5e-6,3e-6, 1e-6,7.5e-7, 5e-7, 3e-7, 1e-7, 7.5e-8, 5e-8, 3e-8, 1e-8, 7.5e-9, 5e-9, 3e-9, 1e-9,
             7.5e-10, 5e-10]

# DOPRI5(4)
A = np.array([[0, 0, 0, 0, 0, 0, 0], [1 / 5, 0, 0, 0, 0, 0, 0], [3 / 40, 9 / 40, 0, 0, 0, 0, 0],
               [44 / 45, -56 / 15, 32 / 9, 0, 0, 0, 0],
               [19372 / 6561, -25360 / 2187, 64448 / 6561, -212 / 729, 0, 0, 0],
               [9017 / 3168, -355 / 33, 46732 / 5247, 49 / 176, -5103 / 18656, 0, 0],
               [35 / 384, 0, 500 / 1113, 125 / 192, -2187 / 6784, 11 / 84, 0]])
b = np.array([35 / 384, 0, 500 / 1113, 125 / 192, -2187 / 6784, 11 / 84, 0])
c = np.array([0, 1 / 5, 3 / 10, 4 / 5, 8 / 9, 1, 1])
s = 7
p = 5
q = 4
b_dach = np.array([5179 / 57600, 0, 7571 / 16695, 393 / 640, -92097 / 339200, 187 / 2100, 1 / 40])

# Referenzlösung berechnen

sol = solve_ivp(fun, [t0, t_end], u0, dense_output=True, args=(x_stern, d, k, m), rtol=10 ** (-12),
                 atol=10 ** (-17),
                 method='RK45')

err1 = []
stepsInTotal1 = []
stepsDone1 = []
err2 = []
stepsInTotal2 = []
stepsDone2 = []
t = np.linspace(0,t_end,500)

for tol in tol_array:
    # Lösung mit Event
    x1, x1_punkt, t1, t_events, steps1 = ODE.solveRKEvent(lambda t, u: fun1(t, u, x_stern, d, k, m),
                                                          lambda t, u: fun2(t, u, x_stern, d, k, m),
                                                          lambda t, u: switch(t, u, x_stern), t0, u0, 10 ** (-3),
                                                          t_end, s, A, b, c, tol, tol, p, q, b_dach)
    err1.append(max(np.absolute(x1(t1) - sol.sol(t1)[0])))
    stepsInTotal1.append(steps1)
    stepsDone1.append(len(t1))

    # Lösung ohne Event
    x2, x2_punkt, t2, steps2 = ODE.solveRK(lambda t, u: fun(t, u, x_stern, d, k, m), t0, u0, 10 ** (-3), t_end, s, A, b,
                                           c, False, tol, tol, p, q, b_dach)
    err2.append(max(np.absolute(x2(t2) - sol.sol(t2)[0])))
    stepsInTotal2.append(steps2)
    stepsDone2.append(len(t2))


plt.semilogy(stepsInTotal2, err2, label='ohne Schaltfunktion',color='r')
plt.semilogy(stepsInTotal1, err1, label='mit Schaltfunktion', color='b')
plt.xlabel('Gesamtzahl der Schritte')
plt.ylabel('Fehler')
plt.legend()
plt.title('Aufwand-Präzisions-Diagramm')

plt.show()

fig, (ax2, ax3) = plt.subplots(1, 2)
h_event = t1[1:] - t1[0:-1]
h = t2[1:] - t2[0:-1]

ax2.semilogy(t2[0:-1],h, label='ohne Schaltfunktion',color='r')
ax2.semilogy(t1[0:-1], h_event, label='mit Schaltfunktion', color='b')
ax2.set_xlabel('Zeit')
ax2.set_ylabel('Schrittweite')
ax2.legend()
ax2.set_title('Schrittweite nach Zeit für Toleranz 5e-10')

plt.title('Abgelehnte Schritte')
rejSteps1 = np.array(stepsInTotal1) - np.array(stepsDone1)
rejSteps2 = np.array(stepsInTotal2) - np.array(stepsDone2)
ax3.semilogx(tol_array, rejSteps2, label='ohne Schaltfunktion',color='r')
ax3.semilogx(tol_array, rejSteps1, label='mit Schaltfunktion', color='b')
ax3.invert_xaxis()
ax3.legend()
ax3.set_xlabel('Toleranz')
ax3.set_ylabel('Anzahl abgelehnte Schritte')

plt.show()

