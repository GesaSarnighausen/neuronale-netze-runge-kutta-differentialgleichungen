from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import numpy as np
from ODEsolveClass import solveRK


# glatte rechte Seite
def fun(t, u, d=2 , k=10, m=1):
    return np.array([u[1], -d/m * u[1] - k/m * u[0] + ((
                    1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t))/m])


# nichtglatte rechte Seite
def fun2(t, u, x_stern, d=2, k=10, m=1):
    if u[0] > x_stern:
        return np.array([u[1], -(k + 10000)/m * u[0] - (d + 2000)/m * u[1] + 10000 * x_stern/m + ((
                    1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t))/m])
    # u[0] <= x_stern
    return np.array([u[1], -k / m * u[0] - d / m * u[1] + (1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t)])



t0 = 0
u0 = [0, 1]
t_end = 1
omega = 1
stepsize = [10 ** (-3), 10 ** (-4), 10 ** (-5)]

x_stern = 0.033
d = 2
k = 10
m = 1
t02 = 0
t_end2 = 1
u02 = [0, 0]

# Referenzlösung berechnen
sol = solve_ivp(fun, [t0, t_end], u0, dense_output=True, args=(omega,), rtol=10 ** (-12), atol=10 ** (-12),
                method='RK45')
sol2 = solve_ivp(fun2, [t02, t_end2], u02, dense_output=True, args=(x_stern, d, k, m), rtol=10 ** (-12),
                 atol=10 ** (-12),
                 method='RK45')

# dreistufiges Runge-Kutta-Fehlberg
A1 = np.array([[0, 0, 0], [1, 0, 0], [1 / 4, 1 / 4, 0]])
b1 = np.array([1 / 2, 1 / 2, 0])
c1 = np.array([0, 1, 1 / 2])
b_dach1 = np.array([1 / 6, 1 / 6, 2 / 3])
s1 = 3

# DOPRI5(4)
A2 = np.array([[0, 0, 0, 0, 0, 0, 0], [1 / 5, 0, 0, 0, 0, 0, 0], [3 / 40, 9 / 40, 0, 0, 0, 0, 0],
               [44 / 45, -56 / 15, 32 / 9, 0, 0, 0, 0],
               [19372 / 6561, -25360 / 2187, 64448 / 6561, -212 / 729, 0, 0, 0],
               [9017 / 3168, -355 / 33, 46732 / 5247, 49 / 176, -5103 / 18656, 0, 0],
               [35 / 384, 0, 500 / 1113, 125 / 192, -2187 / 6784, 11 / 84, 0]])
b2 = np.array([35 / 384, 0, 500 / 1113, 125 / 192, -2187 / 6784, 11 / 84, 0])
c2 = np.array([0, 1 / 5, 3 / 10, 4 / 5, 8 / 9, 1, 1])
b_dach2 = np.array([5179 / 57600, 0, 7571 / 16695, 393 / 640, -92097 / 339200, 187 / 2100, 1 / 40])
s2 = 7


# mit Schrittweitensteuerung
tolerance = [1e-5,5e-6, 1e-6, 5e-7, 1e-7, 5e-8, 1e-8, 5e-9, 1e-9, 5e-10, 1e-10, 5e-11, 1e-11, 5e-12, 1e-12, 5e-13,1e-13]
err1 = []
stepsArray1 = []
err1_nichtglatt = []
stepsArray1_nichtglatt = []
err2 = []
stepsArray2 = []
err2_nichtglatt = []
stepsArray2_nichtglatt = []


for tol in tolerance:
    x1, x1_punkt, t1, steps1 = solveRK(lambda t, u: fun(t, u, omega), t0, u0, 10 ** (-3), t_end, s1, A1, b1, c1,
                                       konstant=False, rtol=tol, atol=tol, p=2, q=3, b_dach=b_dach1)
    x1_nichtglatt, x1_punkt_nichtglatt, t1_nichtglatt, steps1_nichtglatt = \
        solveRK(lambda t, u: fun2(t, u, x_stern, d, k, m), t02, u02,10 ** (-3), t_end2, s1, A1, b1, c1, konstant=False,
                rtol=tol, atol=tol, p=2, q=3, b_dach=b_dach1)

    x2, x2_punkt, t2, steps2 = solveRK(lambda t, u: fun(t, u, omega), t0, u0, 10 ** (-3), t_end, s2, A2, b2, c2,
                                       konstant=False, rtol=tol, atol=tol, p=5, q=4, b_dach=np.array([5179 / 57600, 0,
                                                                                           7571 / 16695, 393 / 640,
                                                                                           -92097 / 339200, 187 / 2100,
                                                                                           1 / 40]))
    x2_nichtglatt, x2_punkt_nichtglatt, t2_nichtglatt, steps2_nichtglatt = \
        solveRK(lambda t, u: fun2(t, u, x_stern, d, k, m), t02, u02, 10 ** (-3), t_end2, s2, A2, b2, c2, konstant=False,
                rtol=tol, atol=tol, p=5, q=4, b_dach=np.array([5179 / 57600, 0, 7571 / 16695, 393 / 640, -92097 / 339200,
                                                    187 / 2100, 1 / 40]))

    stepsArray1.append(steps1)
    stepsArray1_nichtglatt.append(steps1_nichtglatt)
    stepsArray2.append(steps2)
    stepsArray2_nichtglatt.append(steps2_nichtglatt)

    # Fehlerberechnung
    t = np.linspace(1, t_end, 500)
    err1.append(max(np.absolute(x1(t) - sol.sol(t)[0])))
    err1_nichtglatt.append(max(np.absolute(x1_nichtglatt(t) - sol2.sol(t)[0])))
    err2.append(max(np.absolute(x2(t) - sol.sol(t)[0])))
    err2_nichtglatt.append(max(np.absolute(x2_nichtglatt(t) - sol2.sol(t)[0])))

plt.loglog(stepsArray1,err1,label='RK-Fehlberg glatt', color='blue')
plt.loglog(stepsArray1_nichtglatt,err1_nichtglatt,label='RK-Fehlberg nichtglatt', color='blue', linestyle='--')
plt.loglog(stepsArray2,err2,label='DOPRI5(4) glatt', color='orange')
plt.loglog(stepsArray2_nichtglatt,err2_nichtglatt,label='DOPRI5(4) nichtglatt', color='orange', linestyle='--')
plt.legend()
plt.xlabel('Gesamtzahl der Schritte')
plt.ylabel('Fehler')
plt.title('Aufwand-Präzisions-Diagramm für den Ein-Massen-Schwinger')
plt.show()
