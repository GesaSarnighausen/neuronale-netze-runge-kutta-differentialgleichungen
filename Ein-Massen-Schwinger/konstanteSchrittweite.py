from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import numpy as np
from ODEsolveClass import solveRK


# Anfangswertproblem mit glatter rechter Seite
def fun(t, u, d=2 , k=10, m=1):
    return np.array([u[1], -d/m * u[1] - k/m * u[0] + ((
                    1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t))/m])

t0 = 0
u0 = [0, 1]
t_end = 1


stepsize = [10 ** (-3), 8* 10 ** (-4), 6*10 ** (-4), 4*10 ** (-4), 2*10 ** (-4), 10 ** (-4),
            8*10 ** (-5), 6*10 ** (-5), 4*10 ** (-5), 2*10 ** (-5), 10 ** (-5) ]

# Referenzlösung berechnen
sol = solve_ivp(fun, [t0, t_end], u0, dense_output=True, rtol=10 ** (-12), atol=10 ** (-12), method='RK45')


fig, axs = plt.subplots(1, 2)
#fig.suptitle('Ein-Massen-Schwinger mit konstanten Runge-Kutta Verfahren')

# euler 1 stufig
A1 = np.array([[0]])
b1 = np.array([1])
c1 = np.array([0])
s1 = 1

errX = np.zeros(len(stepsize))
errXPunkt = np.zeros(len(stepsize))
i = 0
for h in stepsize:
    x, x_punkt, t = solveRK(lambda t, u: fun(t, u), t0, u0, h, t_end, s1, A1, b1, c1, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[0].loglog(stepsize, errX, label='Eulerverfahren (1-stufig)', color='blue')
axs[0].set_title('glatt')

# Heun 2 stufig
A2 = np.array([[0, 0], [1, 0]])
b2 = np.array([0.5, 0.5])
c2 = np.array([0, 1])
s2 = 2

i = 0
for h in stepsize:
    x, x_punkt, t= solveRK(lambda t, u: fun(t, u), t0, u0, h, t_end, s2, A2, b2, c2, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[0].loglog(stepsize, errX, label='Heunverfahren (2-stufig)', color='orange')


# Nyström 3 stufig
A3 = np.array([[0, 0, 0], [2 / 3, 0, 0], [0, 2 / 3, 0]])
b3 = np.array([1 / 4, 3 / 8, 3 / 8])
c3 = np.array([0, 2 / 3, 2 / 3])
s3 = 3

i = 0
for h in stepsize:
    x, x_punkt, t = solveRK(lambda t, u: fun(t, u), t0, u0, h, t_end, s3, A3, b3, c3, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[0].loglog(stepsize, errX, label='Nyströmverfahren (3-stufig)', color='green')


# klassisches RK-Verfahren 4-stufig
A4 = np.array([[0, 0, 0, 0], [1 / 2, 0, 0, 0], [0, 1 / 2, 0, 0], [0, 0, 1, 0]])
b4 = np.array([1 / 6, 1 / 3, 1 / 3, 1 / 6])
c4 = np.array([0, 1 / 2, 1 / 2, 1])
s4 = 4

i = 0
for h in stepsize:
    x, x_punkt, t = solveRK(lambda t, u: fun(t, u), t0, u0, h, t_end, s4, A4, b4, c4, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[0].loglog(stepsize, errX, label='klass. RK-Verfahren (4-stufig)', color='red')
axs[0].legend()
axs[0].set_xlabel('Schrittweite h')
axs[0].set_ylabel('Fehler')


# Anfangswertproblem mit nichtglatter rechter Seite

def fun2(t, u, x_stern, d, k, m):
    if u[0] > x_stern:
        return np.array([u[1], -(k + 1e4)/m * u[0] - (d + 2e3)/m * u[1] + 1e4 * x_stern/m + ((
                    1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t))/m])
    # u[0] <= x_stern
    return np.array([u[1], -k / m * u[0] - d / m * u[1] + ((1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t))/m])


x_stern = 0.033
d = 2
k = 10
m = 1
t0 = 0
t_end = 1
u0 = [0, 0]

# Referenzlösung berechnen
sol = solve_ivp(fun2, [t0, t_end], u0, dense_output=True, args=(x_stern, d, k, m), rtol=10 ** (-12), atol=10 ** (-12),
                method='RK45')

# euler 1 stufig

errX = np.zeros(len(stepsize))
errXPunkt = np.zeros(len(stepsize))
i = 0
for h in stepsize:
    x, x_punkt, t = solveRK(lambda t, u: fun2(t, u, x_stern, d, k, m), t0, u0, h, t_end, s1, A1, b1, c1, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[1].loglog(stepsize, errX, label='Eulerverfahren (1-stufig)', color='blue')
axs[1].set_title('nichtglatt')

# Heun 2 stufig

i = 0
for h in stepsize:
    x, x_punkt, t = solveRK(lambda t, u: fun2(t, u, x_stern, d, k, m), t0, u0, h, t_end, s2, A2, b2, c2, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[1].loglog(stepsize, errX, label='Heunverfahren (2-stufig)', color='orange')

# Nyström 3 stufig

i = 0
for h in stepsize:
    x, x_punkt, t = solveRK(lambda t, u: fun2(t, u, x_stern, d, k, m), t0, u0, h, t_end, s3, A3, b3, c3, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[1].loglog(stepsize, errX, label='Nyströmverfahren (3-stufig)', color='green')

# klassisches RK-Verfahren 4-stufig

i = 0
for h in stepsize:
    x, x_punkt, t = solveRK(lambda t, u: fun2(t, u, x_stern, d, k, m), t0, u0, h, t_end, s4, A4, b4, c4, konstant=True)
    errX[i] = max(np.absolute(x(t) - sol.sol(t)[0]))
    i += 1

axs[1].loglog(stepsize, errX, label='klass. RK-Verfahren (4-stufig)', color='red')
y_scale1 = axs[0].get_ylim()
y_scale2 = axs[1].get_ylim()
axs[1].set_ylim(min(y_scale1[0],y_scale2[0]), max(y_scale1[1],y_scale2[1]))
axs[0].set_ylim(min(y_scale1[0],y_scale2[0]), max(y_scale1[1],y_scale2[1]))
axs[1].legend()
axs[1].set_xlabel('Schrittweite')
axs[1].set_ylabel('Fehler')

plt.show()

