import numpy as np
from scipy.interpolate import interp1d
import RK
from scipy.optimize import root_scalar


# berechnet die Lösung eines AWPs mit beliebigen s-stufigen RK Verfahren mit Hilfe der Klasse RK.
# Boolean konstant gibt an, ob Schrittweitensteuerung verwendet werden soll (False) oder nicht (True).
# Für konstant = True gibt die Funktion die durch einen kubischen Spline interpolierte Lösung und ihre Ableitung,
# sowie die Zeitpunkte, die das RK-Verfahren verwendet hat, zurück.
# Ist konstant = False gibt sie zusätzlich die Gesamtzahl der durchgeführten Schritte zurück.
# fun ist die rechte Seite des AWPs.
# t0 und u0 sind die Startwerte.
# h ist die Startschrittweite (bei konstant = True bleibt sie so).
# t_end ist der Endpunkt.
# s ist die Stufenanzahl des RK-Verfahrens, A die Verfahrensmatrix, b der Gewichtsvektor und c der Knotenvektor.
# Dabei werden Schrittwiederholungen doppelt gezählt, damit work-precision Diagramme erstellt werden können.
# Ist konstant = False, müssen p (konsistenzordnung des Verfahrens), q (Konsistenzordnung
# des Verfahrens mit b_dach), b_dach (Gewichtsvektor des eingebetteten Verfahrens)
# und rtol und atol (relative bzw. absolute Fehlertoleranz) angegeben werden.

def solveRK(fun, t0, u0, h, t_end, s, A, b, c, konstant, rtol=10 ** (-10), atol=10 ** (-10), p=0, q=0, b_dach=0):
    t = [t0]
    x = [u0[0]]
    x_punkt = [u0[1]]
    sol = RK.RK(lambda t, u: fun(t, u), t0, u0, t_end, h, rtol, atol, s, A, b, c)

    while sol.status == 'running':
        # Falls konstant = False ist, gibt schrittwiederholung an, ob wiederholt werden muss
        # Für konstant = True ist schrittwiederholung immer False.
        (schrittwiederholung, dense) = sol.step(konstant, p, q, b_dach)
        if not (schrittwiederholung):
            t_new = sol.t
            u_new = sol.u
            t.append(t_new)
            x.append(u_new[0])
            x_punkt.append(u_new[1])
    if not konstant:
        return interp1d(np.array(t), np.array(x), kind='cubic'), interp1d(np.array(t), np.array(x_punkt),
                                                                      kind='cubic'), np.array(t), sol.steps_in_total
    # Interpolation der berechneten Punkte
    return interp1d(np.array(t), np.array(x), kind='cubic'), interp1d(np.array(t), np.array(x_punkt),
                                                                      kind='cubic'), np.array(t)


# berechnet die Lösung eines AWPs mit Hilfe von Schrittweitensteuerung und root functions (Verfahren II.10)
# fun1 und fun2 entsprechen f+ und f-, switch entspricht s (Rückgabewert sollte longdouble sein,
# damit Bisektion genau arbeiten kann).
# Ansonsten sind alle Parameter wie in solveRK (konstant ist immer False)
# Zusätzlich zu den Rückgabewerten von oben kommt noch eine Liste der events,
# an denen switch das Vorzeichen ändert, hinzu.

def solveRKEvent(fun1, fun2, switch, t0, u0, h, t_end, s, A, b, c, rtol=10 ** (-10), atol=10 ** (-10), p=0,
                 q=0, b_dach=0):
    switchval = switch(t0, u0)
    # signum speichert Vorzeichen des letzten switch ab
    if switchval >= 0:
        signum = 1
        sol = RK.RK(lambda t, u: fun1(t, u), t0, u0, t_end, h, rtol, atol, s, A, b, c)
    else:
        signum = -1
        sol = RK.RK(lambda t, u: fun2(t, u), t0, u0, t_end, h, rtol, atol, s, A, b, c)

    # t_events speichert Nullstellen von switch ab
    t_events = []

    t = [t0]
    x = [u0[0]]
    x_punkt = [u0[1]]
    t_new = t0
    u_new = u0


    while sol.status == 'running':
        schrittwiederholung, dense = sol.step(False, p, q, b_dach)
        if not (schrittwiederholung):
            t_old = t_new
            u_old = u_new
            t_new = sol.t
            u_new = sol.u
            switchval = switch(t_new, u_new)

            # gleiches Vorzeichen
            if switchval * signum >= 0:
                t.append(t_new)
                x.append(u_new[0])
                x_punkt.append(u_new[1])

            # Vorzeichenwechsel
            else:  # switchval * signum < 0:
                # Nullstelle finden und Punkt an Lösung anhängen
                left, right = bisektion(0, 1, lambda t: switch( np.longdouble(t_old) + np.longdouble(sol.step_size) * t,
                                                                dense(t).astype(np.longdouble)), 1e-13)
                t_stern = t_old + sol.step_size * right
                t_events.append(t_stern)
                t.append(t_stern)
                x.append(dense(right)[0])
                x_punkt.append(dense(right)[1])
                # Neuinitialisierung
                signum = signum * (-1)
                if switchval < 0:
                    sol = RK.RK(lambda t, u: fun2(t, u), t_stern, dense(right), t_end, h, rtol,
                                atol, s, A, b, c, sol.steps_in_total)
                else:  # switchval > 0
                    sol = RK.RK(lambda t, u: fun1(t, u), t_stern, dense(right), t_end, h, rtol,
                                atol, s, A, b, c, sol.steps_in_total)

    # Interpolation der berechneten Punkte
    return interp1d(np.array(t), np.array(x), kind='cubic'), interp1d(np.array(t), np.array(x_punkt), kind='cubic'),\
           np.array(t), np.array(t_events), sol.steps_in_total


def bisektion(a,b, function,tol):
    left = np.longdouble(a)
    right = np.longdouble(b)
    while function(right) * function(left) <= 0:
            if abs(right - left) < tol:
                return left, right
            middle = (right - left)/2 + left
            if function(right) * function(middle) > 0:
                right = middle
            else:
                left = middle
    return False
