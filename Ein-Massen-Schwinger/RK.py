import numpy as np



# Runge-Kutta Instanz für explizite RK-Verfahren

class RK:
    def __init__(self, fun, t0, u0, t_end, first_step, rtol, atol, s, A, b, c, steps_in_total=0):
        # rechte Seite des AWPs
        self.fun = fun
        # aktueller Zeitpunkt, t0 ist Startzeitpunkt
        self.t = t0
        # aktueller Funktionswert, u0 ist Startwert
        self.u = u0
        # t_end ist der Zielzeitpunkt
        self.t_end = t_end
        # aktuelle Schrittweite, first_step ist Startschrittweite
        self.step_size = first_step
        # Stufenanzahl des RK-Verfahrens
        self.s = s
        # Verfahrensmatrix
        self.A = np.array(A)
        # Gewichtsvektor
        self.b = np.array(b)
        # Knotenvektor
        self.c = np.array(c)
        # Angabe, ob RK-Verfahren fertig ist oder noch läuft
        self.status = 'running'
        # vorheriger Zeitpunkt
        self.t_old = None
        # relative Toleranz (nur für Schrittweitensteuerung relevant)
        self.rtol = rtol
        # absolute Toleranz (nur für Schrittweitensteuerung relevant)
        self.atol = atol
        # Gesamtzahl der Schritte (nur für Schrittweitensteuerung relevant)
        self.steps_in_total = steps_in_total

    # Die Funktion step führt einen RK-Schritt aus.
    # Boolean konstant gibt an, ob Schrittweitensteuerung verwendet werden soll (False) oder nicht (True)
    # Die restlichen Parameter sind nur relevant, wenn konstant = False ist:
    #   p entspricht Konsistenzordnung des gewählten RK-Verfahrens
    #   q entspricht Konsistenzordnung des eingebetteten Verfahrens
    #   b_dach ist der Gewichtsvektor des eingebetteten Verfahrens
    #   alpha, alpha_max und alpha_min haben die Funktion wie in Verfahren II.9 beschrieben
    # step gibt einen Boolean zurück, der angibt, ob eine Schrittwiederholung nötig ist.
    # Für konstant = True wird immer False zurückgegeben
    # Falls das Verfahren 7-stufig ist, kann der Denseoutput zurückgegeben werden.
    # Dies ist wichtig, um in ODEsolveClass.solveRKEvent die Nullstellen der Schaltfunktion zu approximieren.

    def step(self, konstant, p, q, b_dach, alpha=0.9, alpha_max=5, alpha_min=0.25):
        # konstante Schrittweite
        if konstant:
            k = np.zeros((self.s, len(self.u)))
            k[0] = self.fun(self.t, self.u)
            if self.t + self.step_size < self.t_end:
                h = self.step_size
            else:
                h = self.t_end - self.t
                self.status = 'finished'
            for i in range(1, self.s):
                u_temp = self.u + h * np.matmul(self.A[i][0:i], k[0:i][:])
                k[i] = self.fun(self.t + self.c[i] * h, u_temp)
            self.t_old = self.t
            self.t = self.t_old + h
            self.u = self.u + h * np.matmul(self.b, k)
            return False, None
        # Schrittweitensteuerung eingebettete Verfahren
        else:
            k = np.zeros((self.s, len(self.u)))
            k[0] = self.fun(self.t, self.u)
            if self.t + self.step_size < self.t_end:
                h = self.step_size
            else:
                h = self.t_end - self.t
                self.step_size = h
                self.status = 'finished'
            for i in range(1, self.s):
                u_temp = self.u + h * np.matmul(self.A[i][0:i], k[0:i][:])
                k[i] = self.fun(self.t + self.c[i] * h, u_temp)
            # Näherungswerte mit mit b und b_dach
            u = self.u + h * np.matmul(self.b, k)
            u_dach = self.u + h * np.matmul(np.array(b_dach), k)
            sk = self.atol * np.ones(len(u)) + self.rtol * np.maximum(self.u, u)
            q_stern = min(p, q)
            err = max(np.divide(np.absolute(u - u_dach), sk))
            err = max(err, 10 ** (-50))
            hneu = min(alpha_max, max(alpha_min, alpha * (1 / err) ** (1 / (q_stern + 1)))) * h

            if err <= 1 or self.status == 'finished':
                if self.s == 7:
                    dense = self.denseoutput(k, self.u)
                else:
                    dense = None
                if self.status == 'finished':
                    self.t_old = self.t
                    self.t = self.t_end
                else:
                    self.t_old = self.t
                    self.t = self.t_old + h
                self.u = u
                schrittwiederholung = False
            else:
                schrittwiederholung = True
                dense = None
            if self.status == 'running':
                self.step_size = hneu
            self.steps_in_total += 1
            return schrittwiederholung, dense


    def denseoutput(self, k, u_start):
        def b_function(theta):
            b = np.zeros(7)
            b[0] = theta**2 * (3 - 2 * theta) * self.b[0] + theta * (theta - 1)**2 - theta**2 * (theta - 1)**2 * 5 *\
                   (2558722523 - 31403016 * theta) / 11282082432
            # b[1] = 0
            b[2] = theta**2 * (3-2*theta) * self.b[2] + theta**2*(theta - 1)**2 * 100 * \
                   (882725551-15701508*theta) / 32700410799
            b[3] = theta**2 * (3-2*theta) * self.b[3] - theta**2*(theta - 1)**2 * 25 * \
                   (443332067-31403016 * theta) / 1880347072
            b[4] = theta**2 * (3-2*theta) * self.b[4] + theta**2*(theta - 1)**2 * 32805 * \
                   (23143187- 3489224 * theta)/199316789632
            b[5] = theta**2 * (3-2*theta) * self.b[5] - theta**2*(theta - 1)**2 * 55 * \
                   (29972135 - 7076736 * theta)/822651844
            b[6] = theta**2 * (theta - 1) + theta**2 * (theta - 1)**2 * 10 * \
                   (7414447 - 829305 * theta)/29380423
            return b
        def dense(theta):
            return u_start + self.step_size * np.matmul(b_function(theta), k)
        return dense

