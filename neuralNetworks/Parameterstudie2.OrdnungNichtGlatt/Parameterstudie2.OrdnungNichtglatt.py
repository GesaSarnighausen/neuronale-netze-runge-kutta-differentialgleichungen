import NN
import torch
import numpy as np
import math




#Parameterstudie
numOfLayers = [3,4,5,6,7]
numOfNeurons = [4,8,16]
batchsize = [10, 100, 1000]

tabelleMax = np.zeros((3,5,3))


# rechte Seite der ODE
def f(x, xpunkt, t, x_stern=0.033, d=2, k=10, m=1):
    cond = x > 0.033
    return -d / m * xpunkt - k / m * x + (
                (1 - (2 * t - 1) ** 2) * torch.sin(2 * math.pi * t)) / m - cond / m * 10 ** 3 * (
                   10 * (x - x_stern) + 2 * xpunkt)


# Referenzlösung berechnen
def fun(t, u, x_stern=0.033, d=2, k=10, m=1):
    cond = u[0] > x_stern
    return np.array([u[1], -k / m * u[0] - d / m * u[1] + (1 - (2 * t - 1) ** 2) * np.sin(
        2 * np.pi * t) - cond * 10 ** 3 * (10 * (u[0] - 0.033) + 2 * u[1])])

start = 0
stop = 1
a1 = 0
a2 = 0

for i in range(len(batchsize)):
    for j in range(len(numOfLayers)):
        for k in range(len(numOfNeurons)):
            print('batchsize:', batchsize[i], 'numOfLayers:', numOfLayers[j], 'numOfNeurons:',numOfNeurons[k])
            maxnorm = 0
            for rep in range(5):
                norm = NN.solve2Ordnung(numOfLayers[j],numOfNeurons[k],batchsize[i], f, a1, a2, fun, start, stop)
                maxnorm += 1/5 * norm

            tabelleMax[i][j][k] = maxnorm
np.savetxt('tabelleMax10.txt',tabelleMax[0])
np.savetxt('tabelleMax100.txt',tabelleMax[1])
np.savetxt('tabelleMax1000.txt',tabelleMax[2])