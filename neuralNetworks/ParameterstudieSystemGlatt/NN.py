from torch import nn, optim
import torch
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

# Die Funktionen solveSystem und solve2Ordnung lösen beide ein Anfangswertproblem 2. Ordnung.
# solveSystem löst das AWP, indem das äquivalente System erster Ordnung gelöst wird und solve2Ordnung löst das AWP direkt.
# Die Parameter sind fast identisch.
# numberOfLayers: Anzahl der Schichten des neuronalen Netzes inklusive Input und Output (hier sind nur Zahlen von 2-7 möglich)
# numberOfNeurons: Anzahl der Neuronen pro Schicht. Alle Schichten haben die gleiche Anzahl an Neuronen
# batch: Anzahl der äquidistant verteilten Trainingsdatenpunkte
# bei solveSystem f1, f2: rechte Seiten des Systems erster Ordnung
# bei solve2Ordnung f: rechte Seite des AWPs
# a1, a2: Anfangsbedingungen der Lösung und der Ableitung im Startzeitpunkt
# fun:  Funktion zur Berechnung der Referenzlösung (siehe scipy.integrate.solve_ivp für Details)
# start: Startzeitpunkt der Lösung
# stop: Endzeitpunkt der Lösung

def solveSystem(numberOfLayers, numberOfNeurons, batch, f1, f2, a1, a2, fun, start, stop):
    torch.set_default_dtype(torch.double)

    # Definition der zwei Netze
    class Network1(nn.Module):
        def __init__(self):
            super(Network1, self).__init__()
            input_size = 1
            output_size = 1
            self.input = nn.Linear(input_size, numberOfNeurons)
            if numberOfLayers > 2:
                self.hidden1 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 3:
                self.hidden2 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 4:
                self.hidden3 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 5:
                self.hidden4 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 6:
                self.hidden5 = nn.Linear(numberOfNeurons, numberOfNeurons)
            self.output = nn.Linear(numberOfNeurons, output_size)

        def forward(self, x):
            x = torch.sigmoid(self.input(x))
            if numberOfLayers > 2:
                x = torch.sigmoid(self.hidden1(x))
            if numberOfLayers > 3:
                x = torch.sigmoid(self.hidden2(x))
            if numberOfLayers > 4:
                x = torch.sigmoid(self.hidden3(x))
            if numberOfLayers > 5:
                x = torch.sigmoid(self.hidden4(x))
            if numberOfLayers > 6:
                x = torch.sigmoid(self.hidden5(x))
            x = self.output(x)
            return x


    class Network2(nn.Module):
        def __init__(self):
            super(Network2, self).__init__()
            input_size = 1
            output_size = 1
            self.input = nn.Linear(input_size, numberOfNeurons)
            if numberOfLayers > 2:
                self.hidden1 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 3:
                self.hidden2 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 4:
                self.hidden3 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 5:
                self.hidden4 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 6:
                self.hidden5 = nn.Linear(numberOfNeurons, numberOfNeurons)
            self.output = nn.Linear(numberOfNeurons, output_size)

        def forward(self, x):
            x = torch.sigmoid(self.input(x))
            if numberOfLayers > 2:
                x = torch.sigmoid(self.hidden1(x))
            if numberOfLayers > 3:
                x = torch.sigmoid(self.hidden2(x))
            if numberOfLayers > 4:
                x = torch.sigmoid(self.hidden3(x))
            if numberOfLayers > 5:
                x = torch.sigmoid(self.hidden4(x))
            if numberOfLayers > 6:
                x = torch.sigmoid(self.hidden5(x))
            x = self.output(x)
            return x


    net1 = Network1()
    net2 = Network2()


    # Definition des Datasets für Trainingsdaten
    class ArrayDataset(torch.utils.data.Dataset):
        def __init__(self, input):
            self.inputdata = input

        def __len__(self):
            return len(self.inputdata)

        def __getitem__(self, item):
            return self.inputdata[item]


    # Referenzlösung berechnen
    t0 = start
    u0 = [a1, a2]
    t_end = stop

    sol = solve_ivp(fun, [t0, t_end], u0, dense_output=True, rtol=10 ** (-12),
                    atol=10 ** (-12),
                    method='RK45')


    batchsize = batch
    input_data = torch.linspace(start, stop, batchsize)
    input_data = torch.unsqueeze(input_data, 1)
    dataset = ArrayDataset(input_data)


    data = torch.utils.data.DataLoader(dataset, batch_size=batchsize, shuffle=False)

    optimizer1 = optim.Adam(net1.parameters(), lr=0.005)
    optimizer2 = optim.Adam(net2.parameters(), lr=0.005)


    epochs = 10000

    running_loss_plot = []
    lossmin = 0

    for epoch in range(epochs):
        running_loss = 0

        for i, batch in enumerate(data, 0):
            # Training
            optimizer1.zero_grad()
            optimizer2.zero_grad()
            inputs = batch

            inputs.requires_grad = True
            output1 = net1(inputs)
            output2 = net2(inputs)
            netstrich1 = torch.autograd.grad(output1, inputs, grad_outputs=torch.ones_like(output1), create_graph=True)[0]
            netstrich2 = torch.autograd.grad(output2, inputs, grad_outputs=torch.ones_like(output2), create_graph=True)[0]
            loesung1 = a1 + (inputs - start) * output1
            loesung2 = a2 + (inputs - start) * output2
            ableitungLoesung1 = output1 + (inputs-start)*netstrich1
            ableitungLoesung2 = output2 + (inputs - start) * netstrich2
            loss1 = sum((ableitungLoesung1 - f1(loesung1, loesung2, inputs)) ** 2)
            loss2 = sum((ableitungLoesung2 - f2(loesung1, loesung2, inputs)) ** 2)
            loss = loss1 + loss2
            loss.backward()
            optimizer1.step()
            optimizer2.step()
            if loss < lossmin or epoch == 0:
                lossmin = loss
                torch.save(net1.state_dict(), "params.pt")

            running_loss += loss.item() / len(data.dataset)
        running_loss_plot.append(running_loss)


    net1.load_state_dict(torch.load("params.pt"))

    t = torch.linspace(start, stop, 100)
    t = torch.unsqueeze(t, 1)
    x_network = a1 * torch.ones_like(t) + (t - start) * net1(t)
    x_network = x_network.detach().numpy()
    x_network = np.squeeze(x_network)
    x = sol.sol(np.linspace(start,stop,100))[0]
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    ax1.set_title('Fehler')
    ax1.set_xlabel('Zeit')
    ax1.semilogy(t, abs(x - x_network), color='blue')
    ax2.set_title('Lösung')
    ax2.plot(t, x, color='blue', label='x')
    ax2.plot(t, x_network, color='red', label='x_net')
    ax2.legend()
    ax3.set_title('loss')
    ax3.set_xlabel('Epochen')
    ax3.semilogy(np.arange(0, len(running_loss_plot)), np.array(running_loss_plot))
    plt.legend()
    plt.show()
    #plt.savefig("EMS"+str(batchsize)+"_"+str(numberOfLayers)+"_"+str(numberOfNeurons)+".png")
    #plt.close()
    return max(abs(x - x_network))


def solve2Ordnung(numberOfLayers, numberOfNeurons, batch, f, a1, a2, fun, start, stop):
    torch.set_default_dtype(torch.double)

    class Network(nn.Module):
        def __init__(self):
            super(Network, self).__init__()
            input_size = 1
            output_size = 1
            self.input = nn.Linear(input_size, numberOfNeurons)
            if numberOfLayers > 2:
                self.hidden1 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 3:
                self.hidden2 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 4:
                self.hidden3 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 5:
                self.hidden4 = nn.Linear(numberOfNeurons, numberOfNeurons)
            if numberOfLayers > 6:
                self.hidden5 = nn.Linear(numberOfNeurons, numberOfNeurons)
            self.output = nn.Linear(numberOfNeurons, output_size)

        def forward(self, x):
            x = torch.sigmoid(self.input(x))
            if numberOfLayers > 2:
                x = torch.sigmoid(self.hidden1(x))
            if numberOfLayers > 3:
                x = torch.sigmoid(self.hidden2(x))
            if numberOfLayers > 4:
                x = torch.sigmoid(self.hidden3(x))
            if numberOfLayers > 5:
                x = torch.sigmoid(self.hidden4(x))
            if numberOfLayers > 6:
                x = torch.sigmoid(self.hidden5(x))
            x = self.output(x)
            return x


    net = Network()


    # Trainingsdaten
    class ArrayDataset(torch.utils.data.Dataset):
        def __init__(self, input):
            self.inputdata = input

        def __len__(self):
            return len(self.inputdata)

        def __getitem__(self, item):
            return self.inputdata[item]

    # Referenzlösung berechnen
    t0 = start
    u0 = [a1, a2]
    t_end = stop

    sol = solve_ivp(fun, [t0, t_end], u0, dense_output=True, rtol=10 ** (-12),
                    atol=10 ** (-12),
                    method='RK45')




    batchsize = batch
    input_data = torch.linspace(start, stop, batchsize)
    input_data = torch.unsqueeze(input_data, 1)
    dataset = ArrayDataset(input_data)


    data = torch.utils.data.DataLoader(dataset, batch_size=batchsize, shuffle=False)

    optimizer = optim.Adam(net.parameters(), lr=0.005)




    epochs = 10000

    running_loss_plot = []
    lossmin = 0

    for epoch in range(epochs):
        running_loss = 0

        for i, batch in enumerate(data, 0):
            # Training pass
            optimizer.zero_grad()
            inputs = batch

            inputs.requires_grad = True
            output = net(inputs)
            netstrich = torch.autograd.grad(output, inputs, grad_outputs=torch.ones_like(output), create_graph=True)[0]
            netZweiStrich = \
            torch.autograd.grad(netstrich, inputs, grad_outputs=torch.ones_like(netstrich), create_graph=True)[0]
            ableitungZweiLoesung = (inputs - start) ** 2 * netZweiStrich + 4 * (inputs - start) * netstrich + 2 * output
            ableitungLoesung = a2*torch.ones_like(inputs) + 2 * (inputs - start) * output + (inputs - start) ** 2 * \
                               netstrich
            loesung = a1*torch.ones_like(inputs) + (inputs - start) * a2 + (inputs - start) ** 2 * output
            loss = sum((ableitungZweiLoesung - f(loesung, ableitungLoesung, inputs)) ** 2)
            loss.backward()
            optimizer.step()

            if loss < lossmin or epoch == 0:
                lossmin = loss
                torch.save(net.state_dict(), "params.pt")

            running_loss += loss.item() / len(data.dataset)

        running_loss_plot.append(running_loss)


    net.load_state_dict(torch.load("params.pt"))

    t = torch.linspace(start, stop, 100)
    t = torch.unsqueeze(t, 1)
    x_network = net(t)*t**2 + t*a2 + a1* torch.ones_like(t)
    x_network = x_network.detach().numpy()
    x_network = np.squeeze(x_network)
    x = sol.sol(np.linspace(start,stop,100))[0]
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    ax1.set_title('Fehler')
    ax1.set_xlabel('Zeit')
    ax1.semilogy(t, abs(x - x_network), color='blue')
    ax2.set_title('Lösung')
    ax2.plot(t, x, color='blue', label='x')
    ax2.plot(t, x_network, color='red', label='x_net')
    ax2.legend()
    ax3.set_title('loss')
    ax3.set_xlabel('Epochen')
    ax3.semilogy(np.arange(0, len(running_loss_plot)), np.array(running_loss_plot))
    plt.legend()
    plt.savefig("EMS"+str(batchsize)+"_"+str(numberOfLayers)+"_"+str(numberOfNeurons)+".png")
    plt.close()
    return max(abs(x - x_network))