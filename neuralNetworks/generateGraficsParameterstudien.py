import NN
import torch
import numpy as np
import math

# Programm zur Erzeugung einer Grafik für das glatte oder nichtglatte System mit einem bestimmten
# Parametersatz


glatt = True

if not glatt:
    # nichtglattes AWP
    # Referenzlösung berechnen
    def fun(t, u, x_stern=0.033, d=2, k=10, m=1):
        cond = u[0] > x_stern
        return np.array([u[1], -k / m * u[0] - d / m * u[1] + (1 - (2 * t - 1) ** 2) * np.sin(
            2 * np.pi * t) - cond * 10 ** 3 * (10 * (u[0] - 0.033) + 2 * u[1])])

    # Definition des äquivalenten Systems erster Ordnung
    def f1(x, x_punkt, t):
        return x_punkt


    def f2(x, xpunkt, t, d=2, k=10, m=1):
        cond = x > 0.033
        return -d / m * xpunkt - k / m * x + (1 - (2 * t - 1) ** 2) * torch.sin(2 * math.pi * t) - cond * 10 ** 3 * (
                10 * (x - 0.033) + 2 * xpunkt)

    # rechte Seite der ODE
    def f(x, xpunkt, t, x_stern=0.033, d=2, k=10, m=1):
        cond = x > 0.033
        return -d / m * xpunkt - k / m * x + (
                    (1 - (2 * t - 1) ** 2) * torch.sin(2 * math.pi * t)) / m - cond / m * 10 ** 3 * (
                       10 * (x - x_stern) + 2 * xpunkt)



else:
    # glattes AWP
    # Referenzlösung berechnen
    def fun(t, u, d=2, k=10, m=1):
        return np.array([u[1], -k / m * u[0] - d / m * u[1] + (1 - (2 * t - 1) ** 2) * np.sin(2 * np.pi * t)])


    # Definition des äquivalenten Systems erster Ordnung
    def f1(x, x_punkt, t):
        return x_punkt


    def f2(x, xpunkt, t, d=2, k=10, m=1):
        return -d / m * xpunkt - k / m * x + (1 - (2 * t - 1) ** 2) * torch.sin(2 * math.pi * t)


    # rechte Seite des AWP
    def f(x, xpunkt, t, d=2, k=10, m=1):
        return -d / m * xpunkt - k / m * x + ((1 - (2 * t - 1) ** 2) * torch.sin(2 * math.pi * t)) / m



start = 0
stop = 1
a1 = 0
a2 = 0
NN.solve2Ordnung(6,16,1000, f, a1, a2, fun, start, stop)
#NN.solveSystem(5,4,10, f1, f2, a1, a2, fun, start, stop)